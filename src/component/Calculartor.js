import { BackgroundChild } from "./Background"
import { ButtonChildren } from "./Button"
import { TextResultChildrend } from "./TextResult"

function Caculator (){
    return(
        <BackgroundChild bgColor="#ffff" fontColor="#0000">
            <TextResultChildrend bgColor="#DADCE3" >0</TextResultChildrend>
            <div>
                <ButtonChildren width="200px">DELL</ButtonChildren>
                <ButtonChildren width="100px">&lt;-</ButtonChildren>
                <ButtonChildren width="100px">/</ButtonChildren>
            </div>
            <div>
                <ButtonChildren width="100px">7</ButtonChildren>
                <ButtonChildren width="100px">8</ButtonChildren>
                <ButtonChildren width="100px">9</ButtonChildren>
                <ButtonChildren width="100px">*</ButtonChildren>
            </div>
            <div>
                <ButtonChildren width="100px">4</ButtonChildren>
                <ButtonChildren width="100px">5</ButtonChildren>
                <ButtonChildren width="100px">6</ButtonChildren>
                <ButtonChildren width="100px">-</ButtonChildren>
            </div>
            <div>
                <ButtonChildren width="100px">1</ButtonChildren>
                <ButtonChildren width="100px">2</ButtonChildren>
                <ButtonChildren width="100px">3</ButtonChildren>
                <ButtonChildren width="100px">+</ButtonChildren>
            </div>
            <div>
                <ButtonChildren width="100px">0</ButtonChildren>
                <ButtonChildren width="100px">.</ButtonChildren>
                <ButtonChildren width="200px">=</ButtonChildren>
            </div>
        </BackgroundChild>
    )
}

export default Caculator