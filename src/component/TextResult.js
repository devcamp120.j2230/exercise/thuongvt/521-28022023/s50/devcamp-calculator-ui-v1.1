import styled from "styled-components";

const TextResultParent = styled.div`
        text-align: right;
        font-size: 80px;
        color: #ffff;
        background: #3A4655;
        padding: 15px;
        margin: 10px 0px;
    `;

export const TextResultChildrend = styled(TextResultParent)`
        font-size: ${prop => prop.fontSize};
        color: ${prop => prop.fontColor};
        background: ${prop => prop.bgColor};
    `
